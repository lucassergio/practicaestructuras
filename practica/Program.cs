﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practica
{
    class Program
    {
        static void Main(string[] args)
        {
            //creacion del archivo vacio
            TextWriter TextoPlano;
            TextoPlano = new StreamWriter("TextoPlano.txt");
            string texto;
            //llenado del archivo
            Console.WriteLine("¿cuantas lineas desea agregar?");
            int n = Convert.ToInt16(Console.ReadLine());
            Console.WriteLine("escriba las palabras");

            for (int i = 0; i < n; i++)
            {
                texto = Console.ReadLine();
                texto = texto + "\n";
                TextoPlano.WriteLine(texto);
            }
            TextoPlano.Close();
            Console.Clear();
            Console.WriteLine("se ha guardado el archivo correctamente");
            Console.ReadKey();
            TextReader lectura;
            //lectura del archivo
            lectura = new StreamReader("TextoPlano.txt");
            string extraccion = lectura.ReadToEnd();
            lectura.Close();
            char[] delimitador = { ' ', ',', '\n', '.' };
            string[] palabras = extraccion.Split(delimitador);

            //ejercicio 1
            Console.WriteLine("TEXTO COMPLETO ");
            Console.WriteLine("__________________________");
            Console.WriteLine(extraccion);
            Console.WriteLine("__________________________");


            Console.WriteLine("\n==========================");
            Console.WriteLine("ejercicio 1");
            Console.WriteLine();

            Queue<String> col = new Queue<string>();
            for (int i = 0; i < palabras.Length; i++)
            {
                if (palabras[i].Length > 5 && palabras[i] != "\n" && palabras[i] != " ")
                {
                    col.Enqueue(palabras[i]);
                }
            }
            while (col.Count > 0)
            {
                Console.WriteLine(col.Dequeue());
            }

            //ejercicio 2
            Console.WriteLine("\n==========================");
            Console.WriteLine("ejercicio 2");
            Stack<String> pila = new Stack<string>();
            for (int i = 0; i < palabras.Length; i++)
            {
                if (palabras[i].Length < 6 && palabras[i] != "\n" && palabras[i] != " ")
                {
                    pila.Push(palabras[i]);
                }
            }
            while (pila.Count > 0)
            {
                Console.WriteLine(pila.Pop());
            }

            //ejercicio 3
            string x = "", x1;
            Console.WriteLine("\n==========================");
            Console.WriteLine("ejercicio 3");
            Console.WriteLine();
            Queue<String> cola = new Queue<string>();
            for (int i = 0; i < palabras.Length; i++)
            {
                x1 = palabras[i];
                if (x1 != "")
                {
                    x = x1.Substring(x1.Length - 1, 1);
                    if (x != "")
                    {
                        if (x != "a" && x != "e" && x != "i" && x != "o" && x != "u" && x != "\n" && x != "")
                        {
                            cola.Enqueue(palabras[i]);
                        }
                    }
                }
            }
            while (cola.Count > 0)
            {
                Console.WriteLine(cola.Dequeue());
            }
            Console.WriteLine("==========================");
            Console.ReadKey();


        }
    }
}
